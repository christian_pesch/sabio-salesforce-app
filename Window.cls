public class Window
{
	public string content {get; set;}
	public string TextResult {get; set;}
public string title {get; set;}


	public string textId = System.currentPagereference().getParameters().get('msg');
	public string key = System.currentPagereference().getParameters().get('info');


	public Window(ApexPages.StandardController controller)
	{
	}

	public PageReference OnLoad()
	{
		System.debug('textId - '+textId);
		System.debug('key - '+key);
		if (textId!=null && key!=null)
		{
			TextRequest();
			title=System.currentPagereference().getParameters().get('title');
		}
		else
			content='Error!\nSome of parameters are incorrect!';
		return null;
	}

	public void TextRequest()
    {
		string TextURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/text/'+textId;
		String authURL;
		SABIO_Customizations__c customSeting = SABIO_Customizations__c.getValues(UserInfo.getOrganizationId());
		System.debug('CS = '+customSeting);
		if(customSeting!=null) {
			if((customSeting.URL__c!=null)&&(customSeting.URL__c!='')) {
				authURL = customSeting.URL__c;
			}
			if((customSeting.realm__c!=null)&&(customSeting.realm__c!='')) {
				authURL += '/' + customSeting.realm__c;
			}
		}
		TextURL = authURL+'/sabio/services/text/'+textId;
    	//system.debug(textId);
    	System.HttpRequest TextRequest = new System.HttpRequest();
    	TextRequest.setMethod('GET');
		TextRequest.setEndpoint(TextURL);
		TextRequest.setHeader('sabio-auth-token', key);
		if (!test.isRunningTest())
		{
			System.HttpResponse TextResponse = new System.Http().send(TextRequest);
			if(TextResponse.getStatus()!='OK') {
				content='Error!\nSome of parameters are incorrect!';
				return;
			}
			TextResult = TextResponse.getBody();
			//system.debug(TextResult);
			Text_Results_Des desTextList = (Text_Results_Des)JSON.deserialize(TextResult, Text_Results_Des.class);
			content=desTextList.data.result.fragments[0].content;
			content=content.replace('src="/', 'src="'+authURL+'/');
		}
    }
}