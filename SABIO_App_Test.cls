@isTest 
private class SABIO_App_Test 
{
    static testMethod void testSetup()
    {
        Test.setMock(HttpCalloutMock.class, new SABIO_HTTP_Mock());

        //Case Con = new Case(Status='New', Origin='Phone', Subject='test', type=null);
        //insert Con;


       // ApexPages.StandardController stdCase =  new     ApexPages.StandardController(Con);

        //SABIO_consoleComponentExtension extension = new SABIO_consoleComponentExtension(stdCase);

        Test.setCurrentPage(Page.SABIO_Setup);
        SABIO_SetupController cont = new SABIO_SetupController();
        cont.customization.cw_SABIO_App__API_key__c = 'us08h2zzemnlt4ibkr6e9w6puvgxy57sd1dg1k1wqqi7tup11';
        cont.customization.cw_SABIO_App__realm__c = 'sabio/services';
        cont.customization.cw_SABIO_App__ticketSearchField__c = 'Subject';
        cont.customization.cw_SABIO_App__URL__c = 'https://telmo.sabiosolo.sabio.de';
        cont.customization.Name = UserInfo.getOrganizationId();
        Test.startTest();
        cont.testConnection();
        Test.stopTest();
        cont.saveCust();
        cont.exitSetup();
        

        //Test.setCurrentPage(Page.SABIO_previewer);
        //Test.setCurrentPage(Page.SABIO_console_component);

    }
    static testMethod void testPreviw()
    {
        Test.setMock(HttpCalloutMock.class, new SABIO_HTTP_Mock());
        
        PageReference pr = Page.SABIO_previewer;
        pr.getParameters().put('cType','contact');
        pr.getParameters().put('cId','1jy27n4pmbq5t');

        
      
   
        cw_SABIO_App__SABIO_Customizations__c customization = new cw_SABIO_App__SABIO_Customizations__c();
        customization.cw_SABIO_App__API_key__c = 'us08h2zzemnlt4ibkr6e9w6puvgxy57sd1dg1k1wqqi7tup11';
        customization.cw_SABIO_App__realm__c = 'sabio/services';
        customization.cw_SABIO_App__ticketSearchField__c = 'Subject';
        customization.cw_SABIO_App__URL__c = 'https://telmo.sabiosolo.sabio.de';
        customization.Name = UserInfo.getOrganizationId();
        insert customization;

        Test.startTest();
            Test.setCurrentPage(pr);

            SABIO_previewerExtension cont = new SABIO_previewerExtension();
        cont.TextRequest();
        
        cont.contentId = '1hrm0ixz7wawo';
            cont.contentType = 'text';
        
         cont.TextRequest();
        Test.stopTest();

        

        //Test.setCurrentPage(Page.SABIO_previewer);
        //Test.setCurrentPage(Page.SABIO_console_component);

    }
    
    static testMethod void testComp()
    {
        Test.setMock(HttpCalloutMock.class, new SABIO_HTTP_Mock());
        Case Con = new Case(Status='New', Origin='Phone', Subject='SABIO', type=null);
        insert Con;
        
        
        //
        //
        //
        //
        
      
   
        cw_SABIO_App__SABIO_Customizations__c customization = new cw_SABIO_App__SABIO_Customizations__c();
        customization.cw_SABIO_App__API_key__c = 'us08h2zzemnlt4ibkr6e9w6puvgxy57sd1dg1k1wqqi7tup11';
        customization.cw_SABIO_App__realm__c = 'sabio/services';
        customization.cw_SABIO_App__ticketSearchField__c = 'Subject';
        customization.cw_SABIO_App__URL__c = 'https://telmo.sabiosolo.sabio.de';
        customization.Name = UserInfo.getOrganizationId();
        insert customization;

        Test.startTest();
          PageReference pr = Page.SABIO_console_component;
        pr.getParameters().put('id','1jy27n4pmbq5t');
Test.setCurrentPage(pr);
        ApexPages.StandardController stdCase =  new     ApexPages.StandardController(Con);

        SABIO_consoleComponentExtension extension = new SABIO_consoleComponentExtension(stdCase);
        extension.docType = 'contact';
        extension.docId = '1jy27n4pmbq5t';
            extension.feedCreate();

            
            extension.docId = '1hrm0ixz7wawo';
            extension.docType = 'text';
        extension.feedCreate();
        
        extension.docId = '10cc934552f481090152f4ed3c761ef5';
        extension.attachDocument();
        extension.pageNum = 1;
extension.pageNavigation();
        extension.pageNum = 0;
extension.pageNavigation();
        extension.pageNum = 100;
extension.pageNavigation();
        extension.backORNext = 'next';
extension.pageNavigation();
        extension.backORNext = 'back';
extension.pageNavigation();
        extension.backORNext = 'begin';
extension.pageNavigation();
        extension.backORNext = 'end';
extension.pageNavigation();
        Test.stopTest();
        
        //Test.setCurrentPage(Page.SABIO_previewer);
        //Test.setCurrentPage(Page.SABIO_console_component);

    }
}