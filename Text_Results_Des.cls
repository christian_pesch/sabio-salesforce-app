public class Text_Results_Des 
{
	/*public class branch
	{
	}*/
	
	public class fragment
	{
		//public string id {get; set;}
		//public string resource {get; set;}
		//public string type {get; set;}
		//public List<branch> branches {get; set;}
		public string content {get; set;}
		public List<string> tags {get; set;}
	}
	public class TextResource
	{
		/*public string Id {get; set;}
		public string resource {get; set;}
		public string type {get; set;}*/
		public List<fragment> fragments {get; set;}
	}
	
	public class Data
	{
		public TextResource result {get;set;}
	}
	
	public Data data  {get;set;}
	
	public static Text_Results_Des parse(String json) 
	{
        return (Text_Results_Des)System.JSON.deserialize(json, Text_Results_Des.class);
	}
}