public class Rest_Api_Des 
{
	public class Data
	{
		public string key {get; set;}
	}
	
	public Data data {get; set;}
	
	public static Rest_Api_Des parse(String json) 
	{
        return (Rest_Api_Des) System.JSON.deserialize(json, Rest_Api_Des.class);
	}
}