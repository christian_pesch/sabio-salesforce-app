public with sharing class Rest_Api 
{

	public string cSearch {get; set;}
	public string answer {get; set;}
	public List<Search_Results_Des.SearchResultResource> LAnswer = new List<Search_Results_Des.SearchResultResource>();
	public List<ResourceList> RL {get; set;}
	public string key {get; set;}	
	public string cont {get; set;}
	public string content {get; set;}
	public string hidden {get; set;}
	public boolean displayPopUp {get; set;}
	public string tags {get; set;}
	
	string URL;
	string auth = 	'{\n"type": "credentials",\n"login": "cloudwharf",\n"key": "Gs04Up4h",\n"fullResponse": true\n}';
	
	public List<Search_Results_Des.SearchResultResource> getLAnswer()
    {
        return LAnswer;
    }
	
	public class ResourceList
	{
		public string title {get;set;}
		public string Id {get;set;}
		public string type {get;set;}
		public string excerpt {get;set;}
		public string content {get; set;}
		public List<string> tags {get; set;}
		public boolean TextVisible {get; set;}
		public boolean DocVisible {get; set;}
	}
	
	public PageReference RenderPopup()
	{
		hidden='hidden';
		displayPopUp=false;
		return null;
	}
	 
	public pagereference doAuthRequest() 
    {   	
		displayPopUp=false;
		hidden='hidden';
    	RL = new List<ResourceList>();
    	displayPopUp=false;
    	string AuthURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/authentication';
   	
		System.HttpRequest AuthRequest = new System.HttpRequest();
		AuthRequest.setBody(auth);
        AuthRequest.setMethod('POST');
        AuthRequest.setHeader('Content-Type', ' application/json');
        AuthRequest.setEndpoint(AuthURL);
        System.HttpResponse AuthResponse = new System.Http().send(AuthRequest);
        string AuthAnswer = AuthResponse.getBody();
        Rest_Api_Des desDataList = (Rest_Api_Des)JSON.deserialize(AuthAnswer, Rest_Api_Des.class);
        key=desDataList.data.key;
		
		return null;
    }
    
    public PageReference doSearchRequest()
    {
    	RL.clear();
    	string SearchURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/search?q='+cSearch+'&filterList=type,tags';
    	System.HttpRequest TRequest = new System.HttpRequest();
		TRequest.setMethod('GET');
		TRequest.setEndpoint(SearchURL);
		TRequest.setHeader('sabio-auth-token', key);
		System.HttpResponse TResponse = new System.Http().send(TRequest);
		string SearchAnswer=TResponse.getBody();
		
		Search_Results_Des desSearchList = (Search_Results_Des)JSON.deserialize(SearchAnswer, Search_Results_Des.class);

		LAnswer = desSearchList.data.result;
		system.debug('L '+desSearchList);
		for (integer i=0;i<LAnswer.size();i++)
		{
			ResourceList bufRl =  new ResourceList();
			bufRl.title=LAnswer[i].title;
			bufRl.Id=LAnswer[i].Id;
			bufRL.excerpt=LAnswer[i].excerpt;
			bufRl.type=LAnswer[i].type;
			if (LAnswer[i].type=='text')
			{
				bufRl.TextVisible=true;
				bufRl.DocVisible=false;

			}
			else
			{
				bufRl.TextVisible=false;
				bufRl.DocVisible=true;
			}
			RL.Add(bufRl);
		}


		return null;
    }
    
    public PageReference TextRequest()
    {
    	tags='TAGS: ';
    	displayPopUp=true;
    	hidden='visible';
    	string TextURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/text/'+cont;
    	System.HttpRequest TextRequest = new System.HttpRequest();
    	TextRequest.setMethod('GET');
		TextRequest.setEndpoint(TextURL);
		TextRequest.setHeader('sabio-auth-token', key);
		System.HttpResponse TextResponse = new System.Http().send(TextRequest);
		string TextResult = TextResponse.getBody();
		Text_Results_Des desTextList = (Text_Results_Des)JSON.deserialize(TextResult, Text_Results_Des.class);
		content=desTextList.data.result.fragments[0].content;
		for (integer i=0;i<desTextList.data.result.fragments[0].tags.size();i++)
			tags+=desTextList.data.result.fragments[0].tags[i]+', ';
		system.debug('TAGS '+tags);
    	return null;
    }
}