public class Search_Results_Des 
{
	public class SearchResultResource
	{
		public string objectType {get;set;}
		public string title {get;set;}
		public string Id {get;set;}
		public string parentId {get;set;}
		public string type {get;set;}
		public string excerpt {get;set;}
		public string resource {get; set;}
		
		//public string content {get; set;}
	}
	
	public class Data
	{
		public List<SearchResultResource> result {get;set;}
		public integer total {get;set;}
		public integer rlimit {get;set;}
		public integer start {get;set;}
		//public filter;
	}
	
	public Data data  {get;set;}
	
	public static Search_Results_Des parse(String json) 
	{
        return (Search_Results_Des)System.JSON.deserialize(json, Search_Results_Des.class);
	}
}