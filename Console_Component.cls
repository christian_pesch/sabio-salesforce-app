Global with sharing class Console_Component
{

    public Integer currentPage {get; set;}
    public Integer kolAllPages {get; set;}
    public Integer numberPage {get; set;}
    public Integer bufNumberPage {get; set;}
    public Integer sizeOnePage {get; set;}
    public Integer totalArticle {get; set;}
    public String strLimit {get; set;}
    public String backORNext {get; set;}
    public boolean showPanelPages {get; set;}
    public boolean showMessages {get; set;}
    public String strMessages {get; set;}
    public String style {get; set;}



    public Integer sizeRL {get; set;}
    public Integer kolPage {get; set;}
    public List<NumPage> numPage {get; set;}
    public String showLeft {get; set;}
    public String showRight {get; set;}
    public String showPanel {get; set;}

    public boolean withParam {get; set;}


    public string cSearch {get; set;}
    public string answer {get; set;}
    public List<Search_Results_Des.SearchResultResource> LAnswer = new List<Search_Results_Des.SearchResultResource>();
    public string key {get; set;}
    public string cont {get; set;}
    public string content {get; set;}
    public List<ResourceList> RL {get; set;}
    public List<ResourceList> RLNum {get; set;}
    public boolean displayPopUp {get; set;}
    public string AuthAnswer {get; set;}
    public string SearchAnswer {get; set;}
    public string TextResult {get; set;}

//VARIABLES
    public Case C {get; set;}
    public boolean bModal{get;set;}
    public String bSpiner{get;set;}
    public String docId{get;set;}
    public String errorString{get;set;}
//REST parameters
    public string authKey{get;set;}
    string searchField{get;set;}
    public string reqURL{get;set;}
    private boolean firstSearch;

    //string auth;// =   '{\n"type": "credentials",\n"login": "cloudwharf",\n"key": "Gs04Up4h",\n"fullResponse": true\n}';

    public PageReference reload() {
      return null;
    }

    public Console_Component(ApexPages.StandardController controller)
    {
      try {

        bModal = false;
        bSpiner = 'slds-show';
        C = new Case();
        C = (Case)controller.getRecord();
        string recordId = C.Id;
        SABIO_Customizations__c customSeting = SABIO_Customizations__c.getValues(UserInfo.getOrganizationId());
        System.debug('CS = '+customSeting);
        if(customSeting!=null) {
            searchField = customSeting.ticketSearchField__c;
            if((customSeting.cw_SABIO_App__URL__c!=null)&&(customSeting.cw_SABIO_App__URL__c!='')) {
                reqURL = customSeting.URL__c;
            } else {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'URL Endpoint not defined!'));
              return;
            }
            if((customSeting.cw_SABIO_App__realm__c!=null)&&(customSeting.cw_SABIO_App__realm__c!='')) {
                reqURL += '/' + customSeting.cw_SABIO_App__realm__c;
            }
            if((customSeting.cw_SABIO_App__ticketSearchField__c!=null)&&(customSeting.cw_SABIO_App__ticketSearchField__c!='')) {
                searchField = customSeting.cw_SABIO_App__ticketSearchField__c;
            } else {
              searchField = 'Subject';
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Case serach field name is empty. Seraching perfomed by \'Subject\' field.'));
            }
            if((customSeting.cw_SABIO_App__API_key__c!=null)&&(customSeting.cw_SABIO_App__API_key__c!='')) {
                authKey = customSeting.cw_SABIO_App__API_key__c;
            } else {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'API auth Key not defined!'));
              return;
            }
        }
        List<sObject> bufC = Database.query('SELECT Id, '+searchField+', Type FROM Case WHERE Id=: recordId LIMIT 1');
        if (!bufC.isEmpty()) {
          C = (Case)bufC[0];
        }
        system.debug('Case object: '+C);

        //cont = System.currentPagereference().getParameters().get('msg');
      //  key = System.currentPagereference().getParameters().get('key');
        //authKey = System.currentPagereference().getParameters().get('key');
        if (cont!=null)
        {
            withParam=true;
            sTextRequest();
        }
        else
        {
            withParam=false;
        }
        numPage = new List<NumPage>();
        displayPopUp=false;
        RL = new List<ResourceList>();
        RLNum = new List<ResourceList>();

        firstSearch = true;
        doSearchRequest();
        firstSearch = false;
        return;
      } catch (Exception ex) {
            System.debug('ERROR. '+ex.getTypeName()+' on line '+ex.getLineNumber()+': '+ex.getMessage());
        }
    }

    public class ResourceList
    {
        public string title {get;set;}
        public string Id {get;set;}
        public string type {get;set;}
        public string excerpt {get;set;}
        public string resource {get; set;}
        public string content {get; set;}
        public boolean TextVisible {get; set;}
        public boolean DocVisible {get; set;}
        public boolean SabioVisible {get; set;}
        public boolean ContactVisible {get; set;}
    }



    public void doSearchRequest() {
      System.debug('doSearchRequest');
      bSpiner = 'slds-show';
        strLimit = '5';
        sizeOnePage = 5;//count Artikles on one page
        numberPage = 1;
        showMessages = false;
        strMessages = '';
        bufNumberPage = 1;
        style = 'top-bar';


        boolean nulltype = false;
        numPage.clear();
        RL.clear();
        RLNum.clear();
        string SearchURL;
        string searchValue = (String) C.get(searchField);
        if ((searchValue != null)&&(firstSearch)) {
            cSearch = searchValue;
        }
        if((cSearch==null)||(cSearch=='') ) {
          cSearch = '*';
        }
        system.debug('Seach value ' + cSearch);



        //SearchURL='https://telmo.sabiosolo.sabio.de/sabio/download?id=10cc934558b423860158f869c3c2679e';
//
SearchURL=reqURL+'/sabio/services/search?filterList=type,branch_ids,tags,creation_date,last_modified,rating,author_id&page=1&start=0&limit='+strLimit+'&q='+cSearch;

//
//&filter=[{"property":"-resource","value":"submission"}]
        if (!nulltype)
        {
            System.HttpRequest TRequest = new System.HttpRequest();
            TRequest.setMethod('GET');
            TRequest.setEndpoint(SearchURL);
            TRequest.setHeader('sabio-auth-token', authKey);
            if (!test.isRunningTest() )
            {
                System.HttpResponse TResponse = new System.Http().send(TRequest);
                SearchAnswer=TResponse.getBody();
                System.debug('Serach req body - '+SearchAnswer);
                Search_Results_Des desSearchList = (Search_Results_Des)JSON.deserialize(SearchAnswer, Search_Results_Des.class);

                LAnswer = desSearchList.data.result;
                totalArticle = desSearchList.data.total;
                system.debug('LANSWER '+LAnswer);
            }
        }
        for (integer i=0;i<LAnswer.size();i++)
        {
            ResourceList bufRl =  new ResourceList();
            bufRl.title=LAnswer[i].title;
            bufRl.Id=LAnswer[i].Id;
            bufRL.excerpt=LAnswer[i].excerpt;
            bufRl.type=LAnswer[i].type;///sabio4/pages/preview.jsf?type=News&id=
            system.debug('type '+LAnswer[i].type);
            if (LAnswer[i].type=='sabio')
            {
            	bufRl.TextVisible=false;
            	bufRl.SabioVisible=true;
                bufRl.DocVisible=false;
                bufRl.ContactVisible=false;
                //cont=LAnswer[i].Id;
            }
            else
            if (LAnswer[i].type=='text')
            {
                bufRl.TextVisible=true;
                bufRl.SabioVisible=false;
                bufRl.DocVisible=false;
                bufRl.ContactVisible=false;
                cont=LAnswer[i].Id;
                //bufRl.content=sTextRequest();
            }
            else
            {
                if (LAnswer[i].resource=='contact')
            	{
            		bufRl.TextVisible=false;
	            	bufRl.SabioVisible=false;
	                bufRl.DocVisible=false;
	                bufRl.ContactVisible=true;
            	}
            	else
            	{
	                bufRl.TextVisible=false;
	                bufRl.SabioVisible=false;
	                bufRl.ContactVisible=false;
	                bufRl.DocVisible=true;
            	}
            }
            RL.Add(bufRl);
        }

//////////
    if (!nulltype){

        kolAllPages = totalArticle/sizeOnePage;
        if(math.mod(totalArticle,sizeOnePage)!=0){
        kolAllPages++;
        }

        if(totalArticle<=sizeOnePage){
        showPanelPages = false;
        kolAllPages = 1;
        }

        if(kolAllPages>1){
        currentPage = 1;
        showPanelPages = true;
        }

    }

      //bSpiner = 'slds-hide';
      //PageReference pageRef = ApexPages.currentPage();
      //pageRef.setRedirect(false);
        //return pageRef;
    }

    public void attachDocument() {
      try{
        String SearchURL=reqURL+'/sabio/services/document/'+docId;

        String docURI;
        String extension;
        String docName;
        Integer size;

                    System.HttpRequest TRequest = new System.HttpRequest();
                    TRequest.setMethod('GET');
                    TRequest.setEndpoint(SearchURL);
                    TRequest.setHeader('sabio-auth-token', authKey);

                        System.HttpResponse TResponse = new System.Http().send(TRequest);
                        if(TResponse.getStatus()=='OK') {
                          SearchAnswer=TResponse.getBody();


                          Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(SearchAnswer);
                          system.debug('SearchAnswer -'+SearchAnswer);
                          docURI = (String)((Map<String, Object>)((Map<String, Object>)m.get('data')).get('result')).get('attachmentUri');
                          extension = (String)((Map<String, Object>)((Map<String, Object>)m.get('data')).get('result')).get('extension');
                          docName = (String)((Map<String, Object>)((Map<String, Object>)m.get('data')).get('result')).get('fileName');
                          size = (Integer)((Map<String, Object>)((Map<String, Object>)m.get('data')).get('result')).get('size');

                        }
                        if(size<6000000) {
                          System.HttpRequest TRequest2 = new System.HttpRequest();
                          TRequest2.setMethod('GET');
                          TRequest2.setEndpoint(docURI);
                          TRequest2.setHeader('sabio-auth-token', authKey);

                              System.HttpResponse TResponse2 = new System.Http().send(TRequest2);
                              if(TResponse2.getStatus()=='OK') {
                                 Attachment newAttach  = new Attachment();
                                  newAttach.ParentId  = C.Id;
                                  newAttach.Body  = TResponse2.getBodyAsBlob();
                                  newAttach.Name = docName;
                                  newAttach.ContentType = TResponse2.getHeader('Content-Type');

                                insert newAttach;


                              }

                        } else {
                          errorString = 'File is too large!';
                            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'File is too large!'));
                          System.debug('Too large');
                        }




      } catch (Exception ex) {
            System.debug('ERROR. '+ex.getTypeName()+' on line '+ex.getLineNumber()+': '+ex.getMessage());
        }

//return null;
      }




    public void TextRequest()
    {
        displayPopUp=true;
        content='';
        string TextURL=reqURL+'/sabio/services/text/'+cont;
        System.HttpRequest TextRequest = new System.HttpRequest();
        TextRequest.setMethod('GET');
        TextRequest.setEndpoint(TextURL);
        TextRequest.setHeader('sabio-auth-token', authKey);
        if (!test.isRunningTest())
        {
            System.HttpResponse TextResponse = new System.Http().send(TextRequest);
            TextResult = TextResponse.getBody();
            Text_Results_Des desTextList = (Text_Results_Des)JSON.deserialize(TextResult, Text_Results_Des.class);
            content=desTextList.data.result.fragments[0].content;
            content=content.replace('src="/', 'src="'+reqURL+'/');
        }

        //return null;
    }

    public string sTextRequest()
    {
        content='';
        string TextURL=reqURL+'/sabio/services/text/'+cont;
        System.HttpRequest TextRequest = new System.HttpRequest();
        TextRequest.setMethod('GET');
        TextRequest.setEndpoint(TextURL);
        TextRequest.setHeader('sabio-auth-token', authKey);
        if (!Test.isRunningTest())
        {
            System.HttpResponse TextResponse = new System.Http().send(TextRequest);
            TextResult = TextResponse.getBody();
            Text_Results_Des desTextList = (Text_Results_Des)JSON.deserialize(TextResult, Text_Results_Des.class);
            content=desTextList.data.result.fragments[0].content;

        }
        cont='';
        return content;
    }


    public class NumPage{
        public String numPage {get;set;}
        public String active {get;set;}
        public NumPage(String numPage, String active){
        this.numPage = numPage;
        this.active = active;
        }
    }


    public void pageNavigation(){
        style = 'top-bar';
        showMessages = false;
        strMessages ='';
        if(numberPage>kolAllPages){
                numberPage = bufNumberPage;
                showMessages = true;
                strMessages = 'Page is Next not found.';
                style = 'top-barError';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Page is Next not found.'));
                //return null;
        }
        if(numberPage<=0){
                numberPage = bufNumberPage;
                showMessages = true;
                strMessages = 'Page is with nubmer 0 not found.';
                style = 'top-barError';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Page is with nubmer 0 not found.'));
              //  return null;
        }


        if(backORNext=='next'){
            if((numberPage!=kolAllPages)&&(numberPage<kolAllPages))
            numberPage++;
            else{
                numberPage = kolAllPages;
                //numberPage = bufNumberPage;
                showMessages = true;
                strMessages = 'Page is Next not found.';
                style = 'top-barError';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Page is Next not found.'));
                //return null;
            }
        }
        else
        if(backORNext=='back'){
           if(numberPage>1)
            numberPage--;
            else{
                numberPage = 1;
                showMessages = true;
                //numberPage = bufNumberPage;
                strMessages = 'Page is with nubmer 0 not fount.';
                style = 'top-barError';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Page is with nubmer 0 not fount.'));
                //return null;
            }
        }
        else
        if(backORNext=='end'){
            numberPage = kolAllPages;
        }
        else
        if(backORNext=='begin'){
            numberPage = 1;
        }






    Integer num = (numberPage*sizeOnePage)-sizeOnePage;
    String start = String.valueOf(num);
    String SearchURL=reqURL+'/sabio/services/search?filterList=type,branch_ids,tags,creation_date,last_modified,rating,author_id&page=1&start='+start+'&limit='+strLimit+'&q='+cSearch;
    //&filter=[{"property":"-resource","value":"submission"}]


            System.HttpRequest TRequest = new System.HttpRequest();
            TRequest.setMethod('GET');
            TRequest.setEndpoint(SearchURL);
            TRequest.setHeader('sabio-auth-token', authKey);
        if (!test.isRunningTest() ){
                System.HttpResponse TResponse = new System.Http().send(TRequest);
                SearchAnswer=TResponse.getBody();
                system.debug('SearchAnswer' + SearchAnswer);

                Search_Results_Des desSearchList = (Search_Results_Des)JSON.deserialize(SearchAnswer, Search_Results_Des.class);

                LAnswer = desSearchList.data.result;
//                totalArticle = desSearchList.data.total;
//                system.debug('totalArticle' + totalArticle);
        }
            RL.clear();
        for (integer i=0;i<LAnswer.size();i++)
        {
            ResourceList bufRl =  new ResourceList();
            bufRl.title=LAnswer[i].title;
            bufRl.Id=LAnswer[i].Id;
            bufRL.excerpt=LAnswer[i].excerpt;
            bufRl.type=LAnswer[i].type;
            system.debug('type '+LAnswer[i].type);
            if (LAnswer[i].type=='sabio')
            {
            	bufRl.TextVisible=false;
            	bufRl.SabioVisible=true;
                bufRl.DocVisible=false;
               	bufRl.ContactVisible=false;
                //cont=LAnswer[i].Id;
            }
            else
            if (LAnswer[i].type=='text')
            {
                bufRl.TextVisible=true;
                bufRl.SabioVisible=false;
                bufRl.DocVisible=false;
                bufRl.ContactVisible=false;
                cont=LAnswer[i].Id;
                //bufRl.content=sTextRequest();
            }
            else
            {
            	if (LAnswer[i].resource=='contact')
            	{
            		bufRl.TextVisible=false;
	            	bufRl.SabioVisible=false;
	                bufRl.DocVisible=false;
	                bufRl.ContactVisible=true;
            	}
            	else
            	{
	                bufRl.TextVisible=false;
	                bufRl.SabioVisible=false;
	                bufRl.ContactVisible=false;
	                bufRl.DocVisible=true;
            	}
            }
            RL.Add(bufRl);
        }


    backORNext='';
    bufNumberPage = numberPage;
    //return null;
    }


}