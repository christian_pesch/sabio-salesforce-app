public class SABIO_previewerExtension {

    public string content {get; set;}
    public string textContent {get; set;}
    public string title {get; set;}
    public boolean success{get;set;}


    public string contentId = System.currentPagereference().getParameters().get('cID');
    public string contentType = System.currentPagereference().getParameters().get('cType');


    public string authKey{get;set;}
    public string reqURL{get;set;}
    public string realm{get;set;}


    public SABIO_previewerExtension() {

    }

    public void TextRequest(){
      success = true;
      try {
        SABIO_Customizations__c customSeting = SABIO_Customizations__c.getValues(UserInfo.getOrganizationId());
        System.debug('CS = '+customSeting);
        if(customSeting!=null) {
            if((customSeting.cw_SABIO_App__URL__c!=null)&&(customSeting.cw_SABIO_App__URL__c!='')) {
                reqURL = customSeting.URL__c;
            } else {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.URL_Endpoint_not_defined));//'URL Endpoint not defined!'
              return;
            }
            if((customSeting.cw_SABIO_App__realm__c!=null)&&(customSeting.cw_SABIO_App__realm__c!='')) {
                reqURL += realm = '/' + customSeting.cw_SABIO_App__realm__c;
            }
            if((customSeting.cw_SABIO_App__API_key__c!=null)&&(customSeting.cw_SABIO_App__API_key__c!='')) {
                authKey = customSeting.cw_SABIO_App__API_key__c;
            } else {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.API_auth_Key_not_defined_in_Setup));//'API auth Key not defined!'
              return;
            }
        }
      } catch (Exception ex) {
            System.debug('ERROR. '+ex.getTypeName()+' on line '+ex.getLineNumber()+': '+ex.getMessage());
      }

      try{
        title = '';
        content = '';
        String searchURL;
        if(contentType=='sabio') {
          searchURL=reqURL+'/message/'+contentId;//+'?sabio-auth-token='+authKey;
        } else if(contentType=='contact') {
            searchURL=reqURL+'/contact/'+contentId;//+'?sabio-auth-token='+authKey;
        } else if(contentType=='text') {
            searchURL=reqURL+'/text/'+contentId;//+'?sabio-auth-token='+authKey;
        } else {
          title = 'HTTP 404';
          content = 'Not Found!';
          success = false;
        }
          System.HttpRequest TextRequest = new System.HttpRequest();
          TextRequest.setMethod('GET');
          TextRequest.setEndpoint(searchURL);
          TextRequest.setHeader('sabio-auth-token', authKey);

          System.HttpResponse TextResponse = new System.Http().send(TextRequest);
          if(TextResponse.getStatus()=='OK') {

              String result = TextResponse.getBody();
              System.debug('Preview content - '+result);
              Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(result);


              if(contentType=='sabio') {
                  if(Test.isRunningTest()) {
                            m = (Map<String, Object>)JSON.deserializeUntyped('');
                        }
                  title = (String)((Map<String, Object>)((Map<String, Object>)m.get('data')).get('result')).get('title');
                  content = (String)((Map<String, Object>)((Map<String, Object>)m.get('data')).get('result')).get('content');
                  content = content.replace('href="', 'target="_blank" href="'+reqURL+'/client/');
                  content = content.replace('src="/', 'src="'+reqURL+'/');
              } else if(contentType=='contact') {
						if(Test.isRunningTest()) {
                            m = (Map<String, Object>)JSON.deserializeUntyped('{"data":{"result":{"id":"1jy27n4pmbq5t","academicTitle":"","additionalJob":"","attachmentUri":"https://telmo.sabiosolo.sabio.de/sabio/services/contact/download/1jy27n4pmbq5t","bookmark":false,"brand":"Miles and More","companyAddress":"Frankfurt a.M.","country":"International","created":"Wed Sep 23 2015 11:25:01 GMT+0200","department":"Partnermanagement","email":"kenny.powers@milesandmore.com","expert":"","fax":"040 85 19 27 10","firstName":"Kenny","gender":"m","imageFileName":"eastbound-down-kenny-powers-jpg_400x400.jpg","imageUri":"https://telmo.sabiosolo.sabio.de/storage/sabio/4374ef494c5303eb014cb857838b69c1/phonebook/profile_1jy27n4pmbq5t.jpg?1443000301262","job":"Ansprechpartner für das Partnermanagement","lastModified":"Mon Oct 17 2016 12:50:36 GMT+0200","lastName":"Powers","mobile":"0172 60 70 145","numberDomestic":"+49 172 60 70 142 ","numberInternational":"","objectType":"ExpertResource","phone":"040 85 19 27 36","profession":"Partnermanager","resource":"contact","service":"","substitute":"Joel Eiglmeier","team":"Hotels","title":"Kenny Powers","userPermission":1}},"references":{},"status":{"code":0,"httpStatus":200,"text":"Request successfully submitted","success":true,"requestId":"to7aotw30a5g"},"success":true}');
                        }

                Map<String, Object> contInfo = ((Map<String, Object>)((Map<String, Object>)m.get('data')).get('result'));
                  title = (String)contInfo.get('title');
                  content = '<div class="x-contact-data"><table><tbody><tr><td colspan="2" class="x-field-label">'+
                  '<img class="x-user-photo" src="'+(String)contInfo.get('imageUri')+'"></td></tr><tr>'+
                  '<td class="x-field-label">Address:</td><td>'+(String)contInfo.get('companyAddress')+
                  '</td></tr><tr><td colspan="2"><hr></td><td></td></tr><tr><th colspan="2">Communication</th></tr><tr>'+
                  '<td class="x-field-label">E-Mail:</td><td>'+(String)contInfo.get('email')+
                  '</td></tr><tr><td class="x-field-label">Fax:</td><td>'+(String)contInfo.get('fax')+'</td></tr><tr>'+
                  '<td class="x-field-label">Phone:</td><td>'+(String)contInfo.get('phone')+
                  '</td></tr><tr><td class="x-field-label">Cellphone:</td><td>'+(String)contInfo.get('mobile')+'</td></tr><tr>'+
                  '<td colspan="2"><hr></td><td></td></tr><tr><th colspan="2">Occupation</th></tr><tr><td class="x-field-label">Department:</td><td>'
                  +(String)contInfo.get('department')+'</td></tr><tr>'+
                  '<td class="x-field-label">Team:</td><td>'+(String)contInfo.get('team')+'</td></tr><tr><td class="x-field-label">Job:</td>'+
                  '<td>'+(String)contInfo.get('job')+
                  '</td></tr><tr><td class="x-field-label">Additional job:</td><td>'+
                  ''+(String)contInfo.get('additionalJob')+
                  '</td></tr><tr><td class="x-field-label">Profession:</td><td>'+(String)contInfo.get('profession')+'</td>'+
                  '</tr><tr><td class="x-field-label">Substitution:</td><td>'+(String)contInfo.get('substitute')+'</td></tr><tr><td class="x-field-label">Brand:</td><td>'
                  +(String)contInfo.get('brand')+'</td></tr><tr><td class="x-field-label">'+
                  'Country:</td><td>'+(String)contInfo.get('country')+
                  '</td></tr><tr><td class="x-field-label">Service:</td><td>'+(String)contInfo.get('service')+'</td></tr><tr><td class="x-field-label">'+
                  'Domestic Number:</td><td>'+(String)contInfo.get('numberDomestic')+'</td></tr><tr><td class="x-field-label">'+
                  'International Number:</td><td>'+(String)contInfo.get('numberInternational')+'</td></tr></tbody></table></div>';
                  content = content.replace('href="', 'target="_blank" href="'+reqURL+'/sabio/services/client/');
                  content = content.replace('src="/', 'src="'+reqURL+'/');
              } else if(contentType=='text') {
                  if(Test.isRunningTest()) {
                            m = (Map<String, Object>)JSON.deserializeUntyped('{"data":{"result":{"id":"1hrm0ixz7wawo","resource":"text","type":"text","branches":[{"id":"1jyd9vou4k8ja","title":"TelMo 1st Level GB","color":"1222FF","theme":"1jy2764p1ksir"}],"objectType":"TextResource","realmId":"4374ef494c5303eb014cb857838b69c1","created":"Tue Nov 17 2015 10:04:57 GMT+0100","createdBy":{"id":"10cc934552b221c60152c02498e5542c","resource":"user","language":"de","login":"joel3","firstname":"Joel","lastname":"Jordan","email":"noreply@sabio.de","active":true,"sabioUser":false,"groups":[{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},{"id":"10cc934552b221c60152efbfa3f81b32","name":"Contentfeed"},{"id":"10cc934552f481090152f631ed567e4b","name":"Redakteure Telmo"}]},"group":{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},"lastModified":"Thu Apr 07 2016 17:25:55 GMT+0200","lastModifiedBy":{"id":"4374ef494c5303eb014cb85785e069cf","resource":"user","language":"de","login":"sabio","firstname":"SABIO","lastname":"Admin","email":"noreply@sabio.de","active":true,"sabioUser":true,"groups":[{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},{"id":"10cc934554dcaed501552fa5e5c365f8","name":"Anwender BMW / MINI CIC 1st. "},{"id":"10cc934552f481090152f63252b77e4e","name":"Anwender Car-Net"},{"id":"10cc9345515d194a01522bb4326f4db4","name":"Anwender M&M"},{"id":"4374ef494c5303eb014cb85785a769c8","name":"Anwender Muster AG"},{"id":"10cc934552b221c60152c0596c675a51","name":"Anwender Signal Iduna"},{"id":"10cc934552b221c60152efbfa3f81b32","name":"Contentfeed"},{"id":"10cc9345507f07d801511ff1d8fa3778","name":"CSM Testing "},{"id":"10cc934552b221c60152c0512c2359d2","name":"Freigeber Telmo"},{"id":"10cc93455236beb60152abd364db586a","name":"Geers-CF-Nutzer"},{"id":"10cc93455236beb60152b1a324580a98","name":"GeersAnwender"},{"id":"10cc93455236beb60152b1a90ed40aa7","name":"GeersRedakteure"},{"id":"10cc934553502a9b0154293d62264b3d","name":"HDI 1st Level"},{"id":"10cc934553502a9b0154293dcc594b40","name":"HDI 2nd Level"},{"id":"10cc934553369e0d0153471b5db55192","name":"OTTO 1.st Level"},{"id":"10cc934553369e0d0153471ba7cf5195","name":"OTTO 2nd Level"},{"id":"10cc9345515d194a01522bb568a44db9","name":"Redakteure & Admins M&M"},{"id":"10cc934554dcaed501552fa8eff8660d","name":"Redakteure BMW / MINI CIC"},{"id":"4374ef494c5303eb014cb85785a069c7","name":"Redakteure Muster AG"},{"id":"10cc934552b221c60152c059aa305a57","name":"Redakteure Signal Iduna"},{"id":"10cc934552f481090152f631ed567e4b","name":"Redakteure Telmo"},{"id":"10cc9345507f07d801513954eba27075","name":"REWE TEST"},{"id":"4374ef494c5303eb014cb85785d869ce","name":"SABIO Admins"},{"id":"10cc93454fb1c61d014fbbdf663b6344","name":"sage.at"},{"id":"10cc934554dcaed50155253aa8da371b","name":"Santander 1st Level"},{"id":"10cc934554dcaed501552540c4d23765","name":"Santander Admin"},{"id":"10cc93455236beb60152557e72f15676","name":"SHW Automotive"},{"id":"10cc934553502a9b0153f133c3387ddf","name":"TelMo 1st Level GB"},{"id":"10cc934553502a9b0153f134a1c07de4","name":"TelMo 2nd Level GB"},{"id":"10cc934553369e0d015342096d5903ab","name":"VELUX-Tester"},{"id":"10cc934553369e0d015341c735ed7c95","name":"Wirecard Testnutzer"}]},"title":"Booking a plan","treePath":"Plans & Options > Overview of rates > Book a rate","paths":[[{"id":"4374ef494c5303eb014cb85785ec69d0","title":"ROOT","defaultBranch":"ts2eil46fsde","branches":[{"id":"ts2eil46fsde","title":"TelMo 1st Level","color":"89FF5E","theme":"qe13vm5floaa"},{"id":"1jyd9vou4k8ja","title":"TelMo 1st Level GB","color":"1222FF","theme":"1jy2764p1ksir"}]},{"id":"10cc9345507f07d80151104df7c965f2","title":"Plans & Options","defaultBranch":"ts2eil46fsde","branches":[{"id":"1jyd9vou4k8ja","title":"TelMo 1st Level GB","color":"1222FF","theme":"1jy2764p1ksir"},{"id":"1hrmi4opkzj1s","title":"TelMo 2nd Level GB","color":"FF3414","theme":"10cc93455434b20a01545860840c42b9"}]},{"id":"10cc9345507f07d8015110540f516618","title":"Overview of rates","defaultBranch":"ts2eil46fsde","branches":[{"id":"1jyd9vou4k8ja","title":"TelMo 1st Level GB","color":"1222FF","theme":"1jy2764p1ksir"},{"id":"1hrmi4opkzj1s","title":"TelMo 2nd Level GB","color":"FF3414","theme":"10cc93455434b20a01545860840c42b9"}]},{"id":"10cc9345507f07d80151105a2ea36663","title":"Book a rate","defaultBranch":"ts2eil46fsde","branches":[{"id":"1jyd9vou4k8ja","title":"TelMo 1st Level GB","color":"1222FF","theme":"1jy2764p1ksir"},{"id":"1hrmi4opkzj1s","title":"TelMo 2nd Level GB","color":"FF3414","theme":"10cc93455434b20a01545860840c42b9"}]}]],"groupPermission":15,"userPermission":1,"otherPermission":1,"rating":0,"bookmark":false,"fragments":[{"objectType":"TextFragmentResource","id":"10cc9345507f07d8015114b09dd25c64","resource":"text","type":"text","branches":[{"id":"1jyd9vou4k8ja","title":"TelMo 1st Level GB","color":"1222FF","theme":"1jy2764p1ksir"}],"objectType":"TextFragmentResource","content":"content","tags":[],"attachmentCount":2,"attachments":[{"objectType":"DocumentResource","id":"10cc9345507f07d8015114b1ccab5c6e","resource":"document","type":"video","objectType":"DocumentResource","title":"Video tutorial_fare request","description":"","createdBy":{"id":"10cc934552b221c60152c02498e5542c","resource":"user","language":"de","login":"joel3","firstname":"Joel","lastname":"Jordan","email":"noreply@sabio.de","active":true,"sabioUser":false,"groups":[{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},{"id":"10cc934552b221c60152efbfa3f81b32","name":"Contentfeed"},{"id":"10cc934552f481090152f631ed567e4b","name":"Redakteure Telmo"}]},"created":"Tue Nov 17 2015 10:06:15 GMT+0100","lastModifiedBy":{"id":"4374ef494c5303eb014cb85785e069cf","resource":"user","language":"de","login":"sabio","firstname":"SABIO","lastname":"Admin","email":"noreply@sabio.de","active":true,"sabioUser":true,"groups":[{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},{"id":"10cc934554dcaed501552fa5e5c365f8","name":"Anwender BMW / MINI CIC 1st. "},{"id":"10cc934552f481090152f63252b77e4e","name":"Anwender Car-Net"},{"id":"10cc9345515d194a01522bb4326f4db4","name":"Anwender M&M"},{"id":"4374ef494c5303eb014cb85785a769c8","name":"Anwender Muster AG"},{"id":"10cc934552b221c60152c0596c675a51","name":"Anwender Signal Iduna"},{"id":"10cc934552b221c60152efbfa3f81b32","name":"Contentfeed"},{"id":"10cc9345507f07d801511ff1d8fa3778","name":"CSM Testing "},{"id":"10cc934552b221c60152c0512c2359d2","name":"Freigeber Telmo"},{"id":"10cc93455236beb60152abd364db586a","name":"Geers-CF-Nutzer"},{"id":"10cc93455236beb60152b1a324580a98","name":"GeersAnwender"},{"id":"10cc93455236beb60152b1a90ed40aa7","name":"GeersRedakteure"},{"id":"10cc934553502a9b0154293d62264b3d","name":"HDI 1st Level"},{"id":"10cc934553502a9b0154293dcc594b40","name":"HDI 2nd Level"},{"id":"10cc934553369e0d0153471b5db55192","name":"OTTO 1.st Level"},{"id":"10cc934553369e0d0153471ba7cf5195","name":"OTTO 2nd Level"},{"id":"10cc9345515d194a01522bb568a44db9","name":"Redakteure & Admins M&M"},{"id":"10cc934554dcaed501552fa8eff8660d","name":"Redakteure BMW / MINI CIC"},{"id":"4374ef494c5303eb014cb85785a069c7","name":"Redakteure Muster AG"},{"id":"10cc934552b221c60152c059aa305a57","name":"Redakteure Signal Iduna"},{"id":"10cc934552f481090152f631ed567e4b","name":"Redakteure Telmo"},{"id":"10cc9345507f07d801513954eba27075","name":"REWE TEST"},{"id":"4374ef494c5303eb014cb85785d869ce","name":"SABIO Admins"},{"id":"10cc93454fb1c61d014fbbdf663b6344","name":"sage.at"},{"id":"10cc934554dcaed50155253aa8da371b","name":"Santander 1st Level"},{"id":"10cc934554dcaed501552540c4d23765","name":"Santander Admin"},{"id":"10cc93455236beb60152557e72f15676","name":"SHW Automotive"},{"id":"10cc934553502a9b0153f133c3387ddf","name":"TelMo 1st Level GB"},{"id":"10cc934553502a9b0153f134a1c07de4","name":"TelMo 2nd Level GB"},{"id":"10cc934553369e0d015342096d5903ab","name":"VELUX-Tester"},{"id":"10cc934553369e0d015341c735ed7c95","name":"Wirecard Testnutzer"}]},"lastModified":"Thu Apr 07 2016 17:25:55 GMT+0200","group":{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},"attachmentUri":"https://telmo.sabiosolo.sabio.de/sabio/services/files/10cc9345507f07d8015114b1ccab5c6e?type=attachment","inlineUri":"https://telmo.sabiosolo.sabio.de/sabio/services/files/10cc9345507f07d8015114b1ccab5c6e?type=inline","fileName":"2015-02-03_16-40-57.mp4","extension":"mp4","checksum":2867795877,"size":3145189,"groupPermission":15,"otherPermission":1,"hidden":false},{"objectType":"DocumentResource","id":"10cc9345507f07d8015114b09dd55c65","resource":"document","type":"video","objectType":"DocumentResource","title":"Video tutorial_fare request","description":"","createdBy":{"id":"10cc934552b221c60152c02498e5542c","resource":"user","language":"de","login":"joel3","firstname":"Joel","lastname":"Jordan","email":"noreply@sabio.de","active":true,"sabioUser":false,"groups":[{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},{"id":"10cc934552b221c60152efbfa3f81b32","name":"Contentfeed"},{"id":"10cc934552f481090152f631ed567e4b","name":"Redakteure Telmo"}]},"created":"Tue Nov 17 2015 10:04:57 GMT+0100","lastModifiedBy":{"id":"4374ef494c5303eb014cb85785e069cf","resource":"user","language":"de","login":"sabio","firstname":"SABIO","lastname":"Admin","email":"noreply@sabio.de","active":true,"sabioUser":true,"groups":[{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},{"id":"10cc934554dcaed501552fa5e5c365f8","name":"Anwender BMW / MINI CIC 1st. "},{"id":"10cc934552f481090152f63252b77e4e","name":"Anwender Car-Net"},{"id":"10cc9345515d194a01522bb4326f4db4","name":"Anwender M&M"},{"id":"4374ef494c5303eb014cb85785a769c8","name":"Anwender Muster AG"},{"id":"10cc934552b221c60152c0596c675a51","name":"Anwender Signal Iduna"},{"id":"10cc934552b221c60152efbfa3f81b32","name":"Contentfeed"},{"id":"10cc9345507f07d801511ff1d8fa3778","name":"CSM Testing "},{"id":"10cc934552b221c60152c0512c2359d2","name":"Freigeber Telmo"},{"id":"10cc93455236beb60152abd364db586a","name":"Geers-CF-Nutzer"},{"id":"10cc93455236beb60152b1a324580a98","name":"GeersAnwender"},{"id":"10cc93455236beb60152b1a90ed40aa7","name":"GeersRedakteure"},{"id":"10cc934553502a9b0154293d62264b3d","name":"HDI 1st Level"},{"id":"10cc934553502a9b0154293dcc594b40","name":"HDI 2nd Level"},{"id":"10cc934553369e0d0153471b5db55192","name":"OTTO 1.st Level"},{"id":"10cc934553369e0d0153471ba7cf5195","name":"OTTO 2nd Level"},{"id":"10cc9345515d194a01522bb568a44db9","name":"Redakteure & Admins M&M"},{"id":"10cc934554dcaed501552fa8eff8660d","name":"Redakteure BMW / MINI CIC"},{"id":"4374ef494c5303eb014cb85785a069c7","name":"Redakteure Muster AG"},{"id":"10cc934552b221c60152c059aa305a57","name":"Redakteure Signal Iduna"},{"id":"10cc934552f481090152f631ed567e4b","name":"Redakteure Telmo"},{"id":"10cc9345507f07d801513954eba27075","name":"REWE TEST"},{"id":"4374ef494c5303eb014cb85785d869ce","name":"SABIO Admins"},{"id":"10cc93454fb1c61d014fbbdf663b6344","name":"sage.at"},{"id":"10cc934554dcaed50155253aa8da371b","name":"Santander 1st Level"},{"id":"10cc934554dcaed501552540c4d23765","name":"Santander Admin"},{"id":"10cc93455236beb60152557e72f15676","name":"SHW Automotive"},{"id":"10cc934553502a9b0153f133c3387ddf","name":"TelMo 1st Level GB"},{"id":"10cc934553502a9b0153f134a1c07de4","name":"TelMo 2nd Level GB"},{"id":"10cc934553369e0d015342096d5903ab","name":"VELUX-Tester"},{"id":"10cc934553369e0d015341c735ed7c95","name":"Wirecard Testnutzer"}]},"lastModified":"Thu Apr 07 2016 17:25:55 GMT+0200","group":{"id":"4374ef494c5303eb014cb85785be69cb","name":"Administratoren Muster AG"},"attachmentUri":"https://telmo.sabiosolo.sabio.de/sabio/services/files/10cc9345507f07d8015114b09dd55c65?type=attachment","inlineUri":"https://telmo.sabiosolo.sabio.de/sabio/services/files/10cc9345507f07d8015114b09dd55c65?type=inline","fileName":"2015-02-03_16-40-57.mp4","extension":"mp4","checksum":2867795877,"size":3145189,"groupPermission":15,"otherPermission":1,"hidden":false}],"submissionCount":0}],"archivesCount":0,"archives":[],"hidden":false}},"references":{},"status":{"code":0,"httpStatus":200,"text":"Request successfully submitted","success":true,"requestId":"1k252txz9aywn"},"events":[{"type":"expiredText","resource":"search","count":0},{"type":"soonExpiredText","resource":"search","count":0},{"type":"priorityMessage","resource":"message","count":29},{"type":"message","resource":"message","count":31},{"type":"submission","resource":"submission","count":8}],"success":true}');
                        }
                  
                title = (String)((Map<String, Object>)((Map<String, Object>)m.get('data')).get('result')).get('title');
                Map<String, Object> m1 = (Map<String, Object>)m.get('data');
                Map<String, Object> m2 = (Map<String, Object>)(m1).get('result');
                List< Object> m3 = (List<Object>)(m2).get('fragments');
                System.debug('SHERLOCK: '+m3);
                String attachments='<p/><h1>Attachments:</h1><p/>';
                for(Object obj: m3){
                  Map<String, Object> m4 = (Map<String, Object>)obj;
                  content += (String)m4.get('content');

                  content = content.replace('href="#!', 'target="_blank" href="'+reqURL+'/client/#!');
                  content = content.replace('href="/', 'target="_blank" href="/'+reqURL+'/client/');
                  content = content.replace('src="/', 'src="'+reqURL+'/').remove(realm);

                  List<Object> m5 = (List<Object>)(m4).get('attachments');
                  for(Object obj2: m5){
                    Map<String, Object> m6 = (Map<String, Object>)obj2;
                    String iconURL = (String)m6.get('type');
                    if(iconURL=='presentation') {
                      iconURL = 'https://telmo.sabiosolo.sabio.de/sabio5/resources/sabio-theme-base/images/resourcetypes/document-powerpoint.png';
                      attachments += '<a target="_blank" href="'+m6.get('inlineUri')+'&sabio-auth-token='+authKey+'"><span><img src="'+iconURL+'"/>&nbsp;'+m6.get('title')+'</span></a>&nbsp; &nbsp;';
                    } else if(iconURL=='video') {
                      iconURL = 'https://telmo.sabiosolo.sabio.de/sabio5/resources/sabio-theme-base/images/resourcetypes/document-video.png';
                      attachments += '<a target="_blank" href="'+m6.get('inlineUri')+'&sabio-auth-token='+authKey+'"><span><img src="'+iconURL+'"/>&nbsp;'+m6.get('title')+'</span></a>&nbsp; &nbsp;';
                    } else if(iconURL=='text') {
                      iconURL = 'https://telmo.sabiosolo.sabio.de/sabio5/resources/sabio-theme-base/images/resourcetypes/document-sabio.png';
                      attachments += '<a target="_blank" href="'+m6.get('inlineUri')+'&sabio-auth-token='+authKey+'"><span><img src="'+iconURL+'"/>&nbsp;'+m6.get('title')+'</span></a>&nbsp; &nbsp;';
                    } else if(iconURL=='sabio') {
                      iconURL = 'https://telmo.sabiosolo.sabio.de/sabio5/resources/sabio-theme-base/images/resourcetypes/message-unconfirmed.png';
                    } else {
                        iconURL = 'https://telmo.sabiosolo.sabio.de/sabio5/resources/sabio-theme-base/images/resourcetypes/document-pdf.png';
                        attachments += '<a target="_blank" href="'+m6.get('inlineUri')+'&sabio-auth-token='+authKey+'"><span><img src="'+iconURL+'"/>'+m6.get('title')+'</span></a>&nbsp; &nbsp;';
                    }
                  }
                }
                content += attachments;

              }
            textContent = content.stripHtmlTags();
          }
        } catch (Exception ex) {
            System.debug('ERROR. '+ex.getTypeName()+' on line '+ex.getLineNumber()+': '+ex.getMessage());
        }
    }

}