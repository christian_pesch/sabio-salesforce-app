Global with sharing class MyTest 
{
//Testing sdffd My local ver 2
    public Integer currentPage {get; set;}
    public Integer kolAllPages {get; set;} 
    public Integer numberPage {get; set;} 
    public Integer sizeOnePage {get; set;}
    public Integer totalArticle {get; set;}
    public String strLimit {get; set;}
    public String backORNext {get; set;}
    public boolean showPanelPages {get; set;}
    public boolean showMessages {get; set;}


    public Integer sizeRL {get; set;}
    public Integer kolPage {get; set;}
    public List<NumPage> numPage {get; set;}
    public String showLeft {get; set;}
    public String showRight {get; set;}
    public String showPanel {get; set;}
    public boolean withParam {get; set;}



    public string cSearch {get; set;}
    public string answer {get; set;}
    public List<Search_Results_Des.SearchResultResource> LAnswer = new List<Search_Results_Des.SearchResultResource>();
    public string key {get; set;}   
    public string cont {get; set;}
    public string content {get; set;}
    public List<ResourceList> RL {get; set;}
    public List<ResourceList> RLNum {get; set;}
    public boolean displayPopUp {get; set;}
    public string AuthAnswer {get; set;}
    public string SearchAnswer {get; set;}
    public string TextResult {get; set;}
    public Case C {get; set;}
    
    boolean firstrun=true;
    string URL;
    string auth =   '{\n"type": "credentials",\n"login": "cloudwharf",\n"key": "Gs04Up4h",\n"fullResponse": true\n}';
    
    public MyTest(ApexPages.StandardController controller)
    {
        C = new Case();
        C=(Case)controller.getRecord();
        List<Case> bufC=[SELECT Type FROM Case WHERE Id=: C.Id];
        if (!bufC.isEmpty())
            C.Type=bufC[0].Type;
        //system.debug(C.Subject);        
    }
    
    /*public List<Search_Results_Des.SearchResultResource> getLAnswer()
    {
        //system.debug(LAnswer);
        return LAnswer;
    }*/
     
    public class ResourceList
    {
        public string title {get;set;}
        public string Id {get;set;}
        public string type {get;set;}
        public string excerpt {get;set;}
        public string content {get; set;}
        public boolean TextVisible {get; set;}
        public boolean DocVisible {get; set;}
    }
    
    public PageReference RenderPopup()
    {
        displayPopUp=false;
        return null;
    }
     
    public pagereference doAuthRequest() 
    {       
        cont = System.currentPagereference().getParameters().get('msg');
        key = System.currentPagereference().getParameters().get('key');
        if (cont!=null)
        {
            withParam=true;
            sTextRequest();
        }
        else
        {   
            withParam=false;
        }
        numPage = new List<NumPage>();
        displayPopUp=false;
        RL = new List<ResourceList>();
        RLNum = new List<ResourceList>();
        showLeft = 'disabled';
        showRight = 'disabled';
        showPanel = 'none';

        string AuthURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/authentication';
    
        System.HttpRequest AuthRequest = new System.HttpRequest();
        AuthRequest.setBody(auth);
        AuthRequest.setMethod('POST');
        AuthRequest.setHeader('Content-Type', ' application/json');
        AuthRequest.setEndpoint(AuthURL);
//        if (!test.isRunningTest())
//      {
            System.HttpResponse AuthResponse = new System.Http().send(AuthRequest);
            AuthAnswer = AuthResponse.getBody();
//      }
        Rest_Api_Des desDataList = (Rest_Api_Des)JSON.deserialize(AuthAnswer, Rest_Api_Des.class);
        key=desDataList.data.key;
        system.debug(key);
        doSearchRequest();
        return null;
    }
    
    public PageReference doSearchRequest()
    {
        strLimit = '5';
        sizeOnePage = 5;//count Artikles on one page
        numberPage = 1;
        
        showMessages = false;

        boolean nulltype=false;
        numPage.clear();
        RL.clear();
        RLNum.clear();
        string SearchURL;
        if (firstrun)
        {
            system.debug('TYPE '+C.Type);
            if (C.Type!=null)
            {
                SearchURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/search?q='+C.Type;
                cSearch=C.Type;
            }
            else
                nulltype=true;
            system.debug('search' +SearchURL);
            firstrun=false;
        }
        else
//          SearchURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/search?q='+cSearch;
            SearchURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/search?_dc=1455212416018&filterList=type,branch_ids,tags,creation_date,last_modified,rating,author_id&page=1&start=0&limit='+strLimit+'&filter=[{"property":"-resource","value":"submission"}]&q='+cSearch;
        if (!nulltype)
        {
            System.HttpRequest TRequest = new System.HttpRequest();
            TRequest.setMethod('GET');
            TRequest.setEndpoint(SearchURL);
            TRequest.setHeader('sabio-auth-token', key);
          if (!test.isRunningTest() )
          {
                System.HttpResponse TResponse = new System.Http().send(TRequest);
                SearchAnswer=TResponse.getBody();
                system.debug('SearchAnswer' + SearchAnswer);
                
                Search_Results_Des desSearchList = (Search_Results_Des)JSON.deserialize(SearchAnswer, Search_Results_Des.class);
    
                LAnswer = desSearchList.data.result;
                totalArticle = desSearchList.data.total;
                system.debug('totalArticle' + totalArticle);
          }
        }
        for (integer i=0;i<LAnswer.size();i++)
        {
            ResourceList bufRl =  new ResourceList();
            bufRl.title=LAnswer[i].title;
            bufRl.Id=LAnswer[i].Id;
            bufRL.excerpt=LAnswer[i].excerpt;
            bufRl.type=LAnswer[i].type;
            if (LAnswer[i].type=='text')
            {
                bufRl.TextVisible=true;
                bufRl.DocVisible=false;
                cont=LAnswer[i].Id;
                //bufRl.content=sTextRequest();
            }
            else
            {
                bufRl.TextVisible=false;
                bufRl.DocVisible=true;
            }
            RL.Add(bufRl);
        }


///////////// new
/////////////

    if (!nulltype){

        kolAllPages = totalArticle/sizeOnePage;
        if(math.mod(totalArticle,sizeOnePage)!=0){
        kolAllPages++;          
        }

        if(totalArticle<=sizeOnePage){
        showPanelPages = false;            
        kolAllPages = 1;
        }

        if(kolAllPages>1){
        currentPage = 1;            
        showPanelPages = true;
        }
        
    }

/////////// old
///////////
        kolPage = RL.size()/5;
        if(math.mod(RL.size(),5)!=0){
        kolPage++;          
        }
        
        if(RL.size()<=5)
        kolPage = 1;
        
        if(kolPage>=2){
            showPanel = 'block';
            showRight = '';
        }
        else {
            showPanel = 'none';
            showRight = 'disabled';
        }

        if(kolPage>1){
            currentNumPage = '1';
            for(integer i=1;i<=kolPage;i++){
                if(i==1)
                numPage.add(new NumPage(String.ValueOf(i),'active'));
                else
                numPage.add(new NumPage(String.ValueOf(i),''));
            }           
        }


        for(integer i=0;i<RL.size();i++){
            RLNum.add(RL[i]);
            if(i==4)
            break;
        }
//////////
        return null;
    }
    
    public PageReference TextRequest()
    {
        displayPopUp=true;
        content='';
        string TextURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/text/'+cont;
        System.HttpRequest TextRequest = new System.HttpRequest();
        TextRequest.setMethod('GET');
        TextRequest.setEndpoint(TextURL);
        TextRequest.setHeader('sabio-auth-token', key);
//      if (!test.isRunningTest())
//      {
            System.HttpResponse TextResponse = new System.Http().send(TextRequest);
            TextResult = TextResponse.getBody();
            Text_Results_Des desTextList = (Text_Results_Des)JSON.deserialize(TextResult, Text_Results_Des.class);
            content=desTextList.data.result.fragments[0].content;
//      }
        
        return null;
    }
    
    public string sTextRequest()
    {
        content='';
        
        string TextURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/text/'+cont;
        System.HttpRequest TextRequest = new System.HttpRequest();
        TextRequest.setMethod('GET');
        TextRequest.setEndpoint(TextURL);
        TextRequest.setHeader('sabio-auth-token', key);
//      if (!test.isRunningTest())
//      {
            System.HttpResponse TextResponse = new System.Http().send(TextRequest);
            TextResult = TextResponse.getBody();
            Text_Results_Des desTextList = (Text_Results_Des)JSON.deserialize(TextResult, Text_Results_Des.class);
            content=desTextList.data.result.fragments[0].content;
//      }
        cont='';
        return content;
    }



////////////////////////////////////////
////////////////////////////////////////
    public String currentNumPage {get; set;}
    public String numTypePage {get; set;}
    public PageReference linkNumPage(){
    RLNum.clear();
    if((numTypePage!='left')&&(numTypePage!='right')){
        Integer num = Integer.valueOf(numTypePage);
        num = (num*5)-4;
        num--;
        Integer inc = 0; 
        for(integer i = num; i<RL.size(); i++){
            inc++;
            RLNum.add(RL[i]);
            if(inc>=5)
            break;
        }
        currentNumPage = numTypePage;
        if(currentNumPage=='1') {showLeft = 'disabled'; showRight = '';}
        else if(currentNumPage == String.valueOf(kolPage)) {showRight = 'disabled'; showLeft = '';}
    }else{
        if(numTypePage=='left'){
            Integer num = Integer.valueOf(currentNumPage);
            num--;
            Integer inc = 0; 
            for(integer i = num; i<RL.size(); i++){
                inc++;
                RLNum.add(RL[i]);
                if(inc>=5)
                break;
            }
            currentNumPage = String.valueOf(num);
            if(currentNumPage=='1') {showLeft = 'disabled'; showRight = '';}
        }
        else{
            if(numTypePage=='right'){
            Integer num = Integer.valueOf(currentNumPage);
            num++;
            Integer inc = 0; 
            for(integer i = num; i<RL.size(); i++){
                inc++;
                RLNum.add(RL[i]);
                if(inc>=5)
                break;
            }
            currentNumPage = String.valueOf(num);
            if(currentNumPage==String.valueOf(kolPage)) {showRight = 'disabled'; showLeft = '';}
            }
        }

    }
    
    for(NumPage num: numPage){
        num.active='';
    }

    for(NumPage num: numPage){
        if(currentNumPage==num.numPage)
        num.active='active';
    }

    return null;
    }



    public class NumPage{
        public String numPage {get;set;}    
        public String active {get;set;} 
        public NumPage(String numPage, String active){
        this.numPage = numPage;
        this.active = active;
        }
    }

////////////////////////////////////////
////////////////////////////////////////

    public PageReference pageNavigation(){
    RL.clear();

        if(numberPage>kolAllPages){
                showMessages = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Page is Next not found.'));
            return null;
        }

        if(backORNext=='next'){
            if((numberPage!=kolAllPages)&&(numberPage<=kolAllPages))
            numberPage++;
            else{
                numberPage = kolAllPages; 
                showMessages = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Page is Next not found.'));
             return null;
            }
        }
        else
        if(backORNext=='back'){
           if(numberPage!=1)
            numberPage--;
            else{
                showMessages = true;                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Page is with nubmer 0 not fount.'));
            return null;
            }            
        }
        else
        if(backORNext=='end'){
            numberPage = kolAllPages;
        }
        else
        if(backORNext=='begin'){
            numberPage = 1;
        }




    Integer num = (numberPage*sizeOnePage)-sizeOnePage;
    String start = String.valueOf(num);
    String SearchURL='https://vertrieb-bkk.demo.sabio.de/sabio4/services/search?_dc=1455212416018&filterList=type,branch_ids,tags,creation_date,last_modified,rating,author_id&page=1&start='+start+'&limit='+strLimit+'&filter=[{"property":"-resource","value":"submission"}]&q='+cSearch;

        
            System.HttpRequest TRequest = new System.HttpRequest();
            TRequest.setMethod('GET');
            TRequest.setEndpoint(SearchURL);
            TRequest.setHeader('sabio-auth-token', key);
        if (!test.isRunningTest() ){
                System.HttpResponse TResponse = new System.Http().send(TRequest);
                SearchAnswer=TResponse.getBody();
                system.debug('SearchAnswer' + SearchAnswer);
                
                Search_Results_Des desSearchList = (Search_Results_Des)JSON.deserialize(SearchAnswer, Search_Results_Des.class);
    
                LAnswer = desSearchList.data.result;
//                totalArticle = desSearchList.data.total;
//                system.debug('totalArticle' + totalArticle);
        }
        for (integer i=0;i<LAnswer.size();i++)
        {
            ResourceList bufRl =  new ResourceList();
            bufRl.title=LAnswer[i].title;
            bufRl.Id=LAnswer[i].Id;
            bufRL.excerpt=LAnswer[i].excerpt;
            bufRl.type=LAnswer[i].type;
            if (LAnswer[i].type=='text')
            {
                bufRl.TextVisible=true;
                bufRl.DocVisible=false;
                cont=LAnswer[i].Id;
                //bufRl.content=sTextRequest();
            }
            else
            {
                bufRl.TextVisible=false;
                bufRl.DocVisible=true;
            }
            RL.Add(bufRl);
        }


    backORNext='';

    return null;
    }


}