public class SABIO_SetupController {

    public cw_SABIO_App__SABIO_Customizations__c customization{get;set;}
    public boolean isConnected{get;set;}
    public List<SelectOption> fields{get;set;}

    public String title{get;set;}
    public String severity{get;set;}

    public SABIO_SetupController(){
        isConnected = false;
        try {
        refreshPage();
        fields = new List<SelectOption>();
        fields = new List<SelectOption>();
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get('Case').getDescribe().Fields.getMap();
        System.debug('#fmap = ' + fMap);
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                System.debug('FD - '+fd.getName());

                    fields.add(new SelectOption(fd.getName(),fd.getLabel()));

            }
        } else {
            fields.add(new SelectOption('Subject','Subject'));
        }
            fields.sort();


            customization = cw_SABIO_App__SABIO_Customizations__c.getValues(UserInfo.getOrganizationId());
            System.debug('CS = '+customization);
            if(customization == null) {
                customization = new cw_SABIO_App__SABIO_Customizations__c();
                customization.Name = UserInfo.getOrganizationId();
            } else {

            }
        } catch (Exception ex) {
            System.debug('ERROR. '+ex.getTypeName()+' on line '+ex.getLineNumber()+': '+ex.getMessage());
        }
    }

    public PageReference exitSetup(){
        PageReference pr = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/0A3');
        pr.setRedirect(true);
        return pr;
    }

    public void refreshPage() {
        title = Label.SABIO_Application_Setup;//'SABIO Application Setup';
        severity = 'info';
    }

    public void saveCust () {
        if(cw_SABIO_App__SABIO_Customizations__c.getValues(UserInfo.getOrganizationId())!=null) {
            update customization;
        } else {
            insert customization;
        }
        title = Label.All_changes_saved;//'All changes saved!';
        severity = 'success';
    }

    public void testConnection() {
        try {
            isConnected = false;
            String SearchURL = customization.cw_SABIO_App__URL__c + '/'
                    + customization.cw_SABIO_App__realm__c +
                    '/search?filterList=type,branch_ids,tags,creation_date,last_modified,rating,author_id&page=1&start=1&limit=1&q=*';
            System.HttpRequest TRequest = new System.HttpRequest();
            TRequest.setMethod('GET');
            TRequest.setEndpoint(SearchURL);
            TRequest.setHeader('sabio-auth-token', customization.cw_SABIO_App__API_key__c);
            System.HttpResponse TResponse = new System.Http().send(TRequest);
            System.debug('TResponse'+TResponse);
            String SearchAnswer = TResponse.getBody();
            if (TResponse.getStatus() == 'OK') {
                isConnected = true;
                title = Label.Connection_succesfull;//'Connection succesfull!';
                severity = 'success';
            } else {
                title = Label.Connection_parameters_is_not_correct;//'Connection parameters is not correct!';
                severity = 'error';
                isConnected = false;
            }
        } catch (Exception ex) {
            title = Label.URL_Endpoint_not_defined;//'URL is not assigned as Remote Site Setting!';
            severity = 'error';
            System.debug('ERROR. '+ex.getTypeName()+' on line '+ex.getLineNumber()+': '+ex.getMessage());
        }
    }
}