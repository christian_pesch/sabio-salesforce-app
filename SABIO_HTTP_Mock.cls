/**
 * Created by CloudWharf on 28.12.2016.
 */

@IsTest
global class SABIO_HTTP_Mock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"Good": "Yes"}');
        res.setStatusCode(200);
        res.setStatus('OK');
        return res;
     }
}