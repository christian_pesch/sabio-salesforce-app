@isTest 
private class Console_Component_Test 
{
    static testMethod void myUnitTest() 
    {
    	Case Con = new Case(Status='New', Origin='Phone', Subject='test', type=null);
    	insert Con;
    	Search_Results_Des SRD = new Search_Results_Des();
		SRD.data = new Search_Results_Des.data();
		SRD.data.result = new List<Search_Results_Des.SearchResultResource>(); 
		Search_Results_Des.SearchResultResource r = new Search_Results_Des.SearchResultResource();
		r.objectType='w';
		r.title='t';
		r.Id='l';
		r.ParentId='';
		r.type='';
		r.resource='';
		r.excerpt='';
		SRD.data.result.add(r);
		SRD.data.total=1;
		SRD.data.rlimit=1;
		SRD.data.start=1;
    	Apexpages.StandardController stdController = new Apexpages.StandardController(Con);
		Console_Component C = new Console_Component(stdController);
		C.C.Subject='TEST';
		C.C.type=null;
		system.debug(C.C.Subject);
		C.LAnswer=SRD.data.result; 
		C.LAnswer[0].type='text';
		C.AuthAnswer='{"data":{"key":"dIwbU9ZL3K53H_y96ojkhuT_fZ4Uh8L6H3udT_d1w8kQWdqgJeh70SDq-vcmyMsBXn--EZ43COvAk_H-9bSSk6I9i7z-7mulUUV12e6ryMMX_DeUq9xnb6VXI22TV4vSGx76AjV4"}}';		
		C.SearchAnswer='{"data":{"total":10,"limit":10}}';
		
		//C.getLAnswer();
		Console_Component.ResourceList RC = new Console_Component.ResourceList();
		RC.title='title';
		RC.Id='Id';
		RC.type='text';
		RC.excerpt='t';
		RC.content='123';
		RC.TextVisible=false;
		RC.DocVisible=false;
		RC.SabioVisible=false;
		RC.ContactVisible=false;
		RC.resource='';
		C.RenderPopup();
		C.currentPage=5;
		C.kolAllPages=123;
		C.numberPage=1;
		C.bufNumberPage=2;
		C.sizeOnePage=5;
		C.totalArticle=256;
		C.strLimit='10';
		C.backORNext='next';
		C.showPanelPages=false;
		C.showMessages=false;
		C.strMessages='';
		C.style='';
		C.cont='';
		system.debug('auth');
		C.doAuthRequest();
		system.debug('search');
		C.doSearchRequest();
		C.LAnswer[0].type='';
		C.C.type='asd';
		C.doAuthRequest();
		C.sizeOnePage=5;
		C.totalArticle=2;
		C.LAnswer[0].type='';
		C.LAnswer[0].resource='contact';
		C.doSearchRequest();
		C.LAnswer[0].type='sabio';
		C.doSearchRequest();
		C.kolAllPages=123;
		C.numberPage=1;
		C.pageNavigation();
		C.kolAllPages=2;
		C.numberPage=10;
		C.pageNavigation();
		Console_Component.NumPage NP = new Console_Component.NumPage('5','1');
		NP.numPage ='';
		NP.active='';
		C.sizeRL=5;
		C.answer='';
		C.cont='';
		C.content='';
		C.TextResult='{"data":{"key":"dIwbU9ZL3K53H_y96ojkhuT_fZ4Uh8L6H3udT_d1w8kQWdqgJeh70SDq-vcmyMsBXn--EZ43COvAk_H-9bSSk6I9i7z-7mulUUV12e6ryMMX_DeUq9xnb6VXI22TV4vSGx76AjV4"}}';
		C.sTextRequest();
		C.kolPage=5;
		C.numPage = new List<Console_Component.NumPage>();
		C.numPage.add(NP);
		C.showLeft='';
		C.showRight='';
		C.showPanel='';
		C.TextRequest();
		Text_Results_Des TRD = new Text_Results_Des();
		TRD.data = new Text_Results_Des.data();
		TRD.data.result = new Text_Results_Des.TextResource();
		Text_Results_Des.fragment f = new Text_Results_Des.fragment();
		f.content='test';
		f.tags=new List<string>();
		f.tags.add('test'); 
		TRD.data.result.fragments =  new List<Text_Results_Des.fragment>();
		TRD.data.result.fragments.add(f);
		Rest_Api_Des RAD = new Rest_Api_Des();
		RAD.data = new Rest_Api_Des.data();
		RAD.data.key='qwe';
		Test.setCurrentPageReference(new PageReference('/apex/window?msg=qwrwrwer&info=qwqweqwe')); 
		Window w = new Window(stdController);
		
		w.OnLoad();
		w.content='';
		w.TextResult=C.TextResult;
		w.title='';
		w.OnLoad();
		w.TextRequest();
    }
}